<?php

class Laptop
{
    protected $merk;
    protected $ram;
    protected $vga;

    public function __construct($merk, $ram, $vga)
    {
        $this->merk = $merk;
        $this->ram = $ram . " GB";
        $this->vga = $vga . " GB";
    }

    public function getRam()
    {
        return $this->ram;
    }

    public function getVga()
    {
        return $this->vga;
    }

    public function getMerk()
    {
        return $this->merk;
    }

    public function printDetail()
    {
        echo "Merk : $this->merk";
        echo "<br>";
        echo "Ram : $this->ram";
        echo "<br>";
        echo "Vga : $this->vga";
        echo "<br>";
    }
}
